package org.example

data class ShopItem (val name: String, var price: Double, var stock: Int) {

    fun substractStock(bought : Int) {
        stock -= bought
    }
}