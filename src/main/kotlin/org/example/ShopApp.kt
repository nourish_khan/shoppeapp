package org.example

//private val itemPrices = mapOf("apple" to 0.60, "orange" to 0.25)

fun main(args : Array<String>) {
    val shop : Shop = Shop()
    shop.getPrice(args)
}

class Shop {
    private val apple = "apple"
    private val orange = "orange"
    private val itemsMap = mapOf(apple to ShopItem(apple, 0.60, 1), orange to ShopItem(orange, 0.25, 2))
    private val orderListeners : MutableList<OrderListener> = mutableListOf(MailService)


    fun getPrice(items : Array<String>): Double {
        notifyOrderListenerReceived()
        var price = 0.0
        var appleCount = 0
        var orangeCount = 0
        for(i in items) {
            val item = i.trim().toLowerCase()
            if(apple.equals(item, true)) {
                appleCount++
            } else if (orange.equals(orange,true)) {
                orangeCount++
            }
            //println("$item is %.2f".format(itemPrices[item.trim().toLowerCase()] ?: 0.0))
        }
        if(appleCount > itemsMap[apple]!!.stock && orangeCount > itemsMap[orange]!!.stock) {
            notifyOrderListenerFailed("Your order cannot be processed." +
                    "You ordered $appleCount apples and $orangeCount oranges, " +
                    "but ${itemsMap[apple]!!.stock} apples and ${itemsMap[orange]!!.stock} oranges are in stock. " +
                    "We apologize for the inconvenience.")
        } else if(appleCount > itemsMap[apple]!!.stock)  {
            notifyOrderListenerFailed("Your order cannot be processed." +
                    "You ordered $appleCount apples, but ${itemsMap[apple]!!.stock} are in stock. " +
                    "We apologize for the inconvenience.")
        } else if (orangeCount > itemsMap[orange]!!.stock) {
            notifyOrderListenerFailed("Your order cannot be processed." +
                    "You ordered $orangeCount oranges, but ${itemsMap[orange]!!.stock} are in stock. " +
                    "We apologize for the inconvenience.")
        } else {
            price += (appleCount/2 * (itemsMap[apple]!!.price)) + (appleCount%2 * itemsMap[apple]!!.price)
            price += (orangeCount/3 * (itemsMap[orange]!!.price * 2)) + (orangeCount % 3 * itemsMap[orange]!!.price)
            notifyOrderListenerProcessed(price, appleCount+orangeCount)
        }

        return price
    }

    fun notifyOrderListenerReceived () {
        for (l in orderListeners) {
            l.orderReceived()
        }
    }

    fun notifyOrderListenerProcessed (price: Double, count: Int) {
        for (l in orderListeners) {
            l.orderProcessed(price, count)
        }
    }

    fun notifyOrderListenerFailed (msg: String) {
        for (l in orderListeners) {
            l.orderFailed(msg)
        }
    }
}



