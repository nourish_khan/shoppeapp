package org.example

import kotlin.random.Random

object MailService : OrderListener {
    override fun orderReceived() {
        println("Order received! Processing...")
    }

    override fun orderProcessed(price: Double, count: Int) {
        println("Order successfully processed!")
        println("Your total is %.2f".format(price))
        println("THe delivery will arrive in %d minutes".format(Random.nextInt(0, count)))
    }

    override fun orderFailed(msg: String) {
        println(msg)
    }
}