package org.example

interface OrderListener {
    fun orderReceived()
    fun orderProcessed(price: Double, count: Int)
    fun orderFailed(msg : String)
}