package org.example

import junit.framework.TestCase

class ShopAppTest : TestCase() {
    val shop = Shop()

    fun testGetPrice() {
        var items = arrayOf<String>("apple", "apple ", "apple", "oranges", "oranges", "oranges")
        assertEquals(1.70, shop.getPrice(items))
        items = arrayOf<String>("apple", "apple", "orange", "orange", "OrAngE", "orange")
        assertEquals(1.35, shop.getPrice(items))
    }
}